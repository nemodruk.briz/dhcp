import Joi from "joi";

const checkDhcp = Joi.object({
  ip: Joi.string()
    .ip({
      version: ["ipv4", "ipv6"],
    })
    .required(),
  mac: Joi.string()
    .regex(/^([0-9a-fA-F]{2}[:-]?){5}([0-9a-fA-F]{2})$/)
    .required(),
});

export default { checkDhcp };
