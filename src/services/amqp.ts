import amqp from "amqplib";
import logger from "../helpers/logger.js";

const EXCHANGE_NAME = "dhcp";
const CONNECTION_URL = "amqp://localhost";
let ch: amqp.Channel;

interface dataSettings {
  mac: string;
  ip: string;
}

export const connect = async () => {
  try {
    const connection = await amqp.connect(CONNECTION_URL);
    logger.log({
      message: "mq connected",
      level: "info",
    });
    const channel = await connection.createChannel();
    logger.log({
      message: "mq channel opened",
      level: "info",
    });
    await channel.assertExchange(EXCHANGE_NAME, "direct");
    logger.log({
      message: "mq exchange asserted",
      level: "info",
    });
    ch = channel;
    process.on("exit", () => {
      ch.close();
      logger.log({
        message: "Closing mq channel",
        level: "info",
      });
    });
  } catch (error) {
    const err = error as Error;
    logger.log({
      message: err.message,
      level: "info",
    });
  }
};

export const publishSettings = async (data: dataSettings, event: string) => {
  ch.publish(EXCHANGE_NAME, event, Buffer.from(JSON.stringify(data)));
};
