import logger from "../helpers/logger.js";
import shemas from "../helpers/validationShemas.js";
import * as Koa from "Koa";


type a = "checkDhcp"
export const validatorMiddleware =
  (shema: a, data: (ctx: Koa.Context) => { [k: string]: string }) =>
    async (ctx: Koa.Context, next: () => Promise<unknown>) => {
      try {
        if (!Object.prototype.hasOwnProperty.call(shemas, shema)) {
          throw new Error(`'${shema}' validator is not exist`);
        }
        await shemas[shema].validateAsync(data(ctx));
        await next();
      } catch (err) {
        const error = err as Error;
        logger.log({
          message: error.message,
          level: "info",
        });
        ctx.status = 400;
        ctx.body = { success: false, error: error.message };
      }
    };
