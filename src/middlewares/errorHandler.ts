import * as Koa from "Koa";
type ErrorWithMessage = {
  message: string;
  statusCode: number;
};
export const errorHandler =
  () => async (ctx: Koa.Context, next: () => Promise<unknown>) => {
    try {
      await next();
    } catch (error) {
      const err = error as ErrorWithMessage;
      ctx.status = err.statusCode || 500;
      ctx.body = {
        success: false,
        error: err.message,
      };
    }
  };
