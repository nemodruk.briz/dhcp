import logger from "../helpers/logger.js";
import * as Koa from "Koa";

export const loggerMiddleware = async (
  ctx: Koa.Context,
  next: () => Promise<unknown>
) => {
  logger.log({
    message: `-->${ctx.request.method} ${ctx.request.url}`,
    level: "info",
  });
  await next();
  logger.log({
    message: `<--${ctx.request.method} ${ctx.request.url} ${ctx.response.status}`,
    level: "info",
  });
};
