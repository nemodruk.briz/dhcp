import Router from "koa-router";
import * as Koa from "Koa";

const healthRoute = new Router();
healthRoute.get("/healthcheck", async (ctx: Koa.Context) => {
  ctx.set({ "Content-Type": "application/json" });
  ctx.body = JSON.stringify({
    success: true,
    message: `Name ${process.env.npm_package_name}, version ${process.env.npm_package_version}`,
  });
});

export default healthRoute;
