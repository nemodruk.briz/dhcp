import Router from "koa-router";
import { deleteDhcp } from "../../controllers/deleteDhcp.js";
import { createDhcp } from "../../controllers/createDhcp.js";
import { getDhcp } from "../../controllers/getDhcp.js";
import * as Koa from "Koa";
import { validatorMiddleware } from "../validatorMiddleware.js";

const dhcpRoute = new Router();
dhcpRoute.get("/sync", async (ctx) => {
  const { status, body } = await getDhcp();
  ctx.status = status;
  ctx.body = body;
});

dhcpRoute.delete("/", async (ctx: Koa.Context) => {
  const id = Number(ctx.request.query.id);
  const { status, body } = await deleteDhcp(id);
  ctx.status = status;
  ctx.body = body;
});

dhcpRoute.post(
  "/",
  validatorMiddleware("checkDhcp", (ctx: Koa.Context) => {
    return { ip: ctx.request.body.ip, mac: ctx.request.body.mac };
  }),
  async (ctx) => {
    const { user, ip, mac } = ctx.request.body;
    const { status, body } = await createDhcp(user, ip, mac);
    ctx.status = status;
    ctx.body = body;
  }
);

export default dhcpRoute;
