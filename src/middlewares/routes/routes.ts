import combineRouters from "koa-combine-routers";
import historyRoute from "./dhcpRoute.js";
import healthRoute from "./healthRoute.js";

export const routes = combineRouters(historyRoute, healthRoute);
