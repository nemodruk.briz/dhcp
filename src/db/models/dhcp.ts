import { BuildOptions, DataTypes, Model, Sequelize } from "sequelize";

export interface DHCPAttributes {
  id?: number;
  user: string;
  ip: string;
  mac: string;
  createdAt?: Date;
  updatedAt?: Date;
}
export interface DHCPModel extends Model<DHCPAttributes>, DHCPAttributes {}
export class DHCP extends Model<DHCPModel, DHCPAttributes> {}

export type DHCPStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): DHCPModel;
};

export function Dhcp(sequelize: Sequelize) {
  return <DHCPStatic>sequelize.define("dhcp", {
    user: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    ip: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    mac: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
}
