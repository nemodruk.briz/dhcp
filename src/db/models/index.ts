import { Sequelize, Dialect } from "sequelize";
import { Dhcp, DHCPStatic } from "./dhcp.js";
import dotenv from "dotenv-defaults";
dotenv.config();

export interface DB {
  sequelize: Sequelize;
  dhcp: DHCPStatic;
}

const dbName = process.env.DB_NAME as string;
const dbUser = process.env.DB_USERNAME as string;
const dbHost = process.env.DB_HOSTNAME;
const dbDriver = process.env.DB_DRIVER as Dialect;
const dbPassword = process.env.DB_PASSWORD;

const sequelize = new Sequelize(dbName, dbUser, dbPassword, {
  host: dbHost,
  dialect: dbDriver,
});

const dhcp = Dhcp(sequelize);

export const db: DB = {
  sequelize,
  dhcp,
};
