"use strict";
import { DataTypes, Model } from "sequelize";

module.exports = (sequelize) => {
  class dhcp extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate() {
      // define association here
    }
  }
  dhcp.init(
    {
      ip: DataTypes.STRING,
      mac: DataTypes.STRING,
      user: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "dhcp",
    }
  );
  return dhcp;
};
