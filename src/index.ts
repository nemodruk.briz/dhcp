import Koa from "Koa";
import bodyparser from "koa-bodyparser";
import dotenv from "dotenv-defaults";
import { loggerMiddleware } from "./middlewares/loggerMiddleware.js";
import logger from "./helpers/logger.js";
import { routes } from "./middlewares/routes/routes.js";
import { errorHandler } from "./middlewares/errorHandler.js";
import { connect } from "./services/amqp.js";

dotenv.config();

const app = new Koa();
app.use(loggerMiddleware);
app.use(bodyparser());
app.use(errorHandler());
app.use(routes());

app.listen(process.env.PORT, () => {
  connect();
  logger.log({
    message: `Server running at port ${process.env.PORT}`,
    level: "info",
  });
});
