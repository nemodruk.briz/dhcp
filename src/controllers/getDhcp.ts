import { db } from "../db/models/index.js";
export const getDhcp = async () => {
  const settings = await db.dhcp.findAll();
  if (!settings) {
    const error = {
      statusCode: 400,
      message: "settings for this id is not found",
    };
    throw error;
  }
  return {
    status: 200,
    body: {
      success: true,
      message: "Success",
      data: settings,
    },
  };
};
