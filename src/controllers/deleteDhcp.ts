import { db } from "../db/models/index.js";

export const deleteDhcp = async (id: number) => {
  await db.dhcp.destroy({
    where: {
      id,
    },
  });
  return {
    status: 200,
    body: {
      success: true,
      message: "dhcp settings deleted",
    },
  };
};
