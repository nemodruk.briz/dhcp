import { db } from "../db/models/index.js";
import { publishSettings } from "..//services/amqp.js";

export const createDhcp = async (user: string, ip: string, mac: string) => {
  const settings = await db.dhcp.findOne({
    where: {
      mac,
      ip,
    },
  });
  if (settings) {
    const error = {
      statusCode: 400,
      message: "settings dhcp with this mac and ip already exists",
    };
    throw error;
  }
  const data = await db.dhcp.create({
    user,
    mac,
    ip,
  });
  publishSettings(
    {
      ip,
      mac,
    },
    "dhcp"
  );
  return {
    status: 201,
    body: {
      success: true,
      message: "success",
      data: {
        user: data.user,
        mac: data.mac,
        ip: data.ip,
      },
    },
  };
};
